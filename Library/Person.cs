﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Person
    {
       private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

       private  string surname;

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        private string middleName;

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


    }
}
