﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Library
{
    public class UserSQl
    {
        string select = "select * from Users";
        string insertSt = "insert into Users" + "(UserFirstName, UserLastName, UserMiddleName, UserTelephone) values(@UserFirstName, @UserLastName, @UserMiddleName, @UserTelephone)";
        string sql = @"Data Source=.\S;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update Users set UserFirstName = @UserFirstName, UserLastName = @UserLastName, UserMiddleName = @UserMiddleName, UserTelephone = @UserTelephone where idUser=@idUser;";
        string removeSt = "delete from Users where idUser = @idUser";
        //string searchUser = "select * from Users where UserLastName like '@UserLastName" ;

        DataTable datatable;

        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public User[] showUsers()
        {            
            User user = new User();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlConnection());
            DataSet set = new DataSet();
            adapter.Fill(set,"Users");
            datatable = set.Tables[0];
            User[] users = new User[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                user = new User();
                row = datatable.Rows[i];
                user.Id = (int) row["idUser"];
                user.Surname = (string) row["UserLastName"];
                user.Name = (string) row["UserFirstName"];
                user.MiddleName = (string)row["UserMiddleName"];
                user.Telephone = (int)row["UserTelephone"];
                users[i] = user;   

            }


            return users;

        }
        public int insert(User user)
        {
         
            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlConnection());
            
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@UserLastName";
            param.Value = user.Surname;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
           
            param = new SqlParameter();
            param.ParameterName = "@UserFirstName";
            param.Value = user.Name;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@UserMiddleName";
            param.Value = user.MiddleName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@UserTelephone";
            param.Value = user.Telephone;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
            }


            sqlConnection().Close();
            return 1;
        }
        public int update(User user , int id)
        {
           
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idUser";
            param.Value = id;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@UserLastName";
            param.Value = user.Surname ;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@UserFirstName";
            param.Value = user.Name;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@UserMiddleName";
            param.Value = user.MiddleName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@UserTelephone";
            param.Value = user.Telephone;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlConnection().Close();
            return 1;
        }
        public int remove( int id)
        {
            User user = new User();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idUser";
            param.Value = id ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);      

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlConnection().Close();
            return 1;
        }
        public int search(string UserLastName)
        {
            User user = new User();
            string searchUser = "select * from Users where UserLastName like '%"+UserLastName+"%'";
            SqlCommand sqlCommand = new SqlCommand(searchUser ,sqlConnection());

            SqlParameter param = new SqlParameter();           

            param = new SqlParameter();
            param.ParameterName = "@UserLastName";
            param.Value = UserLastName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlConnection().Close();
            return 1;
        }
    }
}
