﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class UserBook
    {
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
        string userFirstName;

        public string UserFirstName
        {
            get { return userFirstName; }
            set { userFirstName = value; }
        }
        string userLastName;

        public string UserLastName
        {
            get { return userLastName; }
            set { userLastName = value; }
        }
        DateTime issueDateBook;

        public DateTime IssueDateBook
        {
            get { return issueDateBook; }
            set { issueDateBook = value; }
        }
        DateTime returnDateBook;

        public DateTime ReturnDateBook
        {
            get { return returnDateBook; }
            set { returnDateBook = value; }
        }

    }
}
