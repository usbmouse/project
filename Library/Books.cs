﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Books
    {
       private int idBook;

        public int Id
        {
            get { return idBook; }
            set { idBook = value; }
        }
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
        int publicationDateBook;

        public int PublicationDateBook
        {
            get { return publicationDateBook; }
            set { publicationDateBook = value; }
        }

        string authorLastName;

        public string AuthorLastName
        {
            get { return authorLastName; }
            set { authorLastName = value; }
        }

        public Books[] showBooks(Book[] books)
        {
            Books booksts = new Books();
            Books[] masbooksts = new Books[books.Length];


            for (int i = 0; i < books.Length; i++)
            {
                booksts = new Books();
                booksts.Id = books[i].Id;
                booksts.TitleBook = books[i].TitleBook;
                booksts.PublicationDateBook = books[i].PublicationDateBook;
                booksts.AuthorLastName = books[i].Author.Surname;

                masbooksts[i] = booksts;
            }
            return masbooksts;
        }

       
    }
}
