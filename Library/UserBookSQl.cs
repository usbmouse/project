﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Library
{
    class UserBookSQl
    {
        string select = "select TitleBook,issueDateBook,UserFirstName from Books, logBook, Users where Books.IDBook = logBook.IDBook and Users.IDUser = logBook.IDUser";
        string insertSt = "insert into Books" + "(TitleBook,PublicationDateBook) values(@TitleBook,@PublicationDateBook)";
        string sql = "Data Source=MY_COMPUTER;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update Books set TitleBook = @TitleBook, PublicationDateBook =@PublicationDateBook where idBook=@idBook;";
        string removeSt = "delete from Books where idBook = @idBook";
        //string searchBook = "select * from Books where TitleBook like '%:TitleBook%'" ;


        DataTable datatable;
        DataTable datatable1;
        DataTable datatable2;

        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public Book[] showBooks(SqlConnection sqlc)
        {
            
            Book book = new Book();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlc);
            DataSet set = new DataSet();
            adapter.Fill(set,"Books");
            adapter.Fill(set, "logBook");
            adapter.Fill(set, "Users");

            datatable = set.Tables[0];
            datatable1 = set.Tables[1];
            datatable2 = set.Tables[2];

           

            for (int i = 0; i < datatable1.Rows.Count; i++)
            {
                
               // book = new Book();
                
                //book.Id = (int) row["idBook"];
               // book.PublicationDateBook = (int) row["PublicationDateBook"];
               
                



                

            }

            return books;

        }
        public int insert(SqlConnection sqlc,string title, int publicationDateBook)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlc);
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@TitleBook";
            param.Value =title ;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@PublicationDateBook";
            param.Value = publicationDateBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
               // MessageBox.Show(es.Message);
              //  return;
            }

            sqlc.Close();

            return 1;
        }
        public int update(SqlConnection sqlc, string title, int publicationDateBook,int id)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlc);

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idBook";
            param.Value = id;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@TitleBook";
            param.Value = title;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@PublicationDateBook";
            param.Value = publicationDateBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                // MessageBox.Show(es.Message);
                //  return;
            }

            sqlc.Close();

            return 1;
        }
        public int remove(SqlConnection sqlc, int id)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlc);

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idBook";
            param.Value = id ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                // MessageBox.Show(es.Message);
                //  return;
            }

            sqlc.Close();

            return 1;
        }
        public Book[] search(SqlConnection sqlc, string title)
        {

            string searchBook = "select * from Books where TitleBook like '%"+title+"%'";
            Book book = new Book();

            SqlDataAdapter adapter = new SqlDataAdapter(searchBook, sqlc);
            DataSet set = new DataSet();
            adapter.Fill(set, "Books");
            datatable = set.Tables[0];
            Book[] books = new Book[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                book = new Book();
                row = datatable.Rows[i];
                book.Id = (int)row["idBook"];
                book.PublicationDateBook = (int)row["PublicationDateBook"];
                book.TitleBook = (string)row["TitleBook"];

                books[i] = book;

            }

            return books;
       
        }
    }
}
