﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Book
    {
      private  int idBook;

        public int Id
        {
            get { return idBook; }
            set { idBook = value; }
        }
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
        int publicationDateBook;

        public int PublicationDateBook
        {
            get { return publicationDateBook; }
            set { publicationDateBook = value; }
        }

        Author author = new Author();

        public Author Author
        {
            get { return author; }
            set { author = value; }
        }
        

    }
}
