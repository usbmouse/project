﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace Library
{
   public class JournalSQl
    {
        string select = "select * from LogBook";
        string insertSt = "insert into LogBook" + "(IssueDateBook, ReturnDateBook, IDBook, IDUser) values(@IssueDateBook, @ReturnDateBook, @IDBook, @IDUser)";
        string sql = @"Data Source=.\S;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update LogBook set IssueDateBook = @IssueDateBook, ReturnDateBook = @ReturnDateBook, IDBook = @IDBook, IDUser = @IDUser where IDLog = @IDLog;";
        string removeSt = "delete from LogBook where IDLog = @IDLog";
        string searchBook = "select * from LogBook where IDBook like @IDBook";


        DataTable datatable;


        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public BooksUsers [] showJournal(Book[] books,User[] users)
        {
            
            Journal journal = new Journal();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlConnection());
            DataSet set = new DataSet();
            adapter.Fill(set,"LogBook");
            datatable = set.Tables[0];
            Journal[] journals = new Journal[datatable.Rows.Count];
            DataRow row;
            BooksUsers bu = new BooksUsers();
            BooksUsers[] bus = new BooksUsers[datatable.Rows.Count ];
            
            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                journal = new Journal();
                row = datatable.Rows[i];
                journal.Id = (int) row["IDLog"];
                journal.IssueDateBook = (DateTime) row["IssueDateBook"];
                journal.ReturnDateBook = (DateTime) row["ReturnDateBook"];
                journal.IdBook = (int) row["IDBook"];
                journal.IdUser = (int) row["IDUser"];
                journals[i] = journal;   

            }
            for (int i = 0; i < datatable.Rows.Count;i++ )
            {
                bu = new BooksUsers();
                for (int k = 0; k < books.Length; k++)
                {
                    if (journals[i].IdBook == books[k].Id)
                    {
                        //journals[i].TitleBook = books[k].TitleBook;
                        bu.TitleBook = books[k].TitleBook;
                        bu.AuthorLastName = books[k].Author.Surname;      

                    }
                }
                for (int m = 0; m < users.Length; m++)
                {
                    if (journals[i].IdUser == users[m].Id)
                    {
                        //journals[i].UserFirstName  = users[m].Surname ;
                        bu.Name = users[m].Name;
                        bu.Surname = users[m].Surname;
                        bu.MiddleName = users[m].MiddleName;
                        
                    }

                }
                bu.IdLog  = journals[i].Id;
                bu.IssueDateBook = journals[i].IssueDateBook;
                bu.ReturnDateBook = journals[i].ReturnDateBook;
                bus[i] = bu;


            }



            return bus;

        }
        public int insert( DateTime IssueDateBook, DateTime ReturnDateBook, int IDBook, int IDUser)
        {
            Journal journal = new Journal();
            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlConnection());
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@IssueDateBook";
            param.Value = IssueDateBook ;
            param.SqlDbType = SqlDbType.DateTime;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@ReturnDateBook";
            param.Value = ReturnDateBook;
            param.SqlDbType = SqlDbType.DateTime;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@IDBook";
            param.Value = IDBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@IDUser";
            param.Value = IDUser;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
              //  return;
            }

            sqlConnection().Close();

            return 1;
        }
        public int update( DateTime IssueDateBook, DateTime ReturnDateBook, int IDBook, int IDUser, int IDLog)
        {
            Journal journal = new Journal();
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlConnection());
            
            SqlParameter param = new SqlParameter();
            param = new SqlParameter();
            param.ParameterName = "@IDLog";
            param.Value = IDLog;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@IssueDateBook";
            param.Value = IssueDateBook;
            param.SqlDbType = SqlDbType.DateTime;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@ReturnDateBook";
            param.Value = ReturnDateBook;
            param.SqlDbType = SqlDbType.DateTime;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@IDBook";
            param.Value = IDBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@IDUser";
            param.Value = IDUser;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
                //  return;
            }

            sqlConnection().Close();

            return 1;
        }
        public int remove( int id)
        {
            Journal journal = new Journal();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@IDLog";
            param.Value = id ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                // MessageBox.Show(es.Message);
                //  return;
            }

            sqlConnection().Close();

            return 1;
        }
        public int search(int IDBook)
        {
            Journal journal = new Journal();
            SqlCommand sqlCommand = new SqlCommand(searchBook ,sqlConnection());

            SqlParameter param = new SqlParameter();           

            param = new SqlParameter();
            param.ParameterName = "@IDBook";
            param.Value = IDBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();         

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                // MessageBox.Show(es.Message);
                //  return;
            }

            sqlConnection().Close();

            return 1;
        }
    }
}
