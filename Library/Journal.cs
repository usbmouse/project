﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
   public class Journal
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        DateTime issueDateBook;

        public DateTime IssueDateBook
        {
            get { return issueDateBook; }
            set { issueDateBook = value; }
        }
        DateTime returnDateBook;

        public DateTime ReturnDateBook
        {
            get { return returnDateBook; }
            set { returnDateBook = value; }
        }
        int idBook;

        public int IdBook
        {
            get { return idBook; }
            set { idBook = value; }
        }
        int idUser;

        public int IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }
        Book book = new Book();

        public Book Book
        {
            get { return book; }
            set { book = value; }
        }


        User user = new User();

        public User User
        {
            get { return user; }
            set { user = value; }
        }
        string userFirstName;

        public string UserFirstName
        {
            get { return userFirstName; }
            set { userFirstName = value; }
        }
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
       

    }
}
