﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            Book[] books;
            Author author = new Author();
            Author[] authors;
            BookSQl booksql = new BookSQl();
           if ((textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" ))
           {
                Show();
                MessageBox.Show("Заполните пустые поля");
           }
           else
           {
            book.TitleBook = textBox1.Text;
            book.PublicationDateBook = Int32.Parse(textBox2.Text);
            books = booksql.showBooks();
            //book.Id = books[books.Length - 1].Id + 1;     
            
            AuthorSQl authorsql = new AuthorSQl();
            book.Author.Surname = textBox3.Text;
            book.Author.Name = textBox4.Text;
            book.Author.MiddleName = textBox5.Text;
            authors = authorsql.showAuthors();
            //book.Author.Id = authors[authors.Length - 1].Id + 1;
            //booksql.insert(book);
            authorsql.insert(book.Author);
            booksql.insert(book);
            Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
    }
}
