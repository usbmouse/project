﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            Books books = new Books();
              //dataGridView1.DataSource = booksql.showBooks();
            bindingSource1.DataSource = books.showBooks (booksql.showBooks());
            dataGridView1.DataSource = bindingSource1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            Form3 form = new Form3();
            form.Show();

           /* string title;
           
            
            title =textBox1.Text  ;
            int PublicationDateBook;
            PublicationDateBook = Int32.Parse(textBox3.Text); 

            booksql.insert(booksql.sqlConnection(),title,PublicationDateBook);*/
            //dataGridView1.DataSource = booksql.showBooks();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            Book book = new Book();
            //book.TitleBook = textBox2.Text;
           // book.PublicationDateBook = Int32.Parse(textBox3.Text);
            int id;
            id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value;

            //booksql.update(book,id);
            Form4 form = new Form4(id);
            form.Show();
            //dataGridView1.DataSource = booksql.showBooks();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            
            int id;

            Books books = new Books();
            //DataRow datarow = new DataRow();

            id= (int) dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value ;

        

            booksql.remove(id);
            dataGridView1.DataSource = books.showBooks(booksql.showBooks());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            Books books = new Books();
            string title;
            title = textBox1.Text;
            bindingSource1.DataSource = books.showBooks(booksql.search(title)); //booksql.search(title);
            dataGridView1.DataSource = bindingSource1;


        }

        private void form_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }

        private void jToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            Books books = new Books();
           // bindingSource1.DataSource = books.showBooks(booksql.showBooks());
            //dataGridView1.DataSource = bindingSource1;
            dataGridView1.DataSource = books.showBooks(booksql.showBooks());
            //books.showBooks();
        }

        
    }
}
