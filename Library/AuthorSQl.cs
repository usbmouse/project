﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace Library
{
    public class AuthorSQl
    {
        string select = "select * from Authors order by IDAuthor";
        string insertSt = "insert into Authors" + "(AuthorFirstName, AuthorLastName, AuthorMiddleName) values(@AuthorFirstName, @AuthorLastName, @AuthorMiddleName);";
        string sql = @"Data Source=.\S;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update Authors set AuthorFirstName = @AuthorFirstName, AuthorLastName = @AuthorLastName, AuthorMiddleName = @AuthorMiddleName where idAuthor=@idAuthor;";
        string removeSt = "delete from Authors where idAuthor = @idAuthor";
        //string searchUser = "select * from Authors where AuthorLastName like @AuthorLastName";
    
        DataTable datatable;


        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public Author[] showAuthors()
        {
            
            Author author = new Author();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlConnection());
            DataSet set = new DataSet();
            adapter.Fill(set,"Authors");
            datatable = set.Tables[0];
            Author[] authors = new Author[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                author = new Author();
                row = datatable.Rows[i];
                author.Id = (int) row["idAuthor"];
                author.Surname = (string) row["AuthorLastName"];
                author.Name = (string) row["AuthorFirstName"];
                author.MiddleName = (string)row["AuthorMiddleName"];
                authors[i] = author;   

            }

            return authors;

        }
        public int insert(Author author)
        {
            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlConnection());
            
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@AuthorLastName";
            param.Value = author.Surname;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
           
            param = new SqlParameter();
            param.ParameterName = "@AuthorFirstName";
            param.Value =author.Name;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@AuthorMiddleName";
            param.Value = author.MiddleName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
 
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
             }

            sqlConnection().Close();

            return 1;
        }
        public int update(Author author, int id)
        {
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idAuthor";
            param.Value = id;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@AuthorLastName";
            param.Value = author.Surname;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            
            param = new SqlParameter();
            param.ParameterName = "@AuthorFirstName";
            param.Value = author.Name;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@AuthorMiddleName";
            param.Value = author.MiddleName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
                
            }

            sqlConnection().Close();

            return 1;
        }
        public int remove(int id)
        {
            Author author = new Author();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idAuthor";
            param.Value = id ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);      

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
             }

            sqlConnection().Close();

            return 1;
        }
        public int search(string AuthorLastName)
        {
            Author author = new Author();

            string searchUser = "select * from Authors where AuthorLastName like '%" + AuthorLastName + "%'";
            SqlCommand sqlCommand = new SqlCommand(searchUser , sqlConnection());

            SqlParameter param = new SqlParameter();           

            param = new SqlParameter();
            param.ParameterName = "@AuthorLastName";
            param.Value = AuthorLastName;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlConnection().Close();

            return 1;
        }

        
    }
}
