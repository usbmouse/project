﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Form4 : Form
    {
        public Form4(int i)
        {
            InitializeComponent();
            n = i;
        }
        int n,i;

        private void button1_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            Book[] books;
            Author author = new Author();
            Author[] authors;
            BookSQl booksql = new BookSQl();

            if ((textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == ""))
            {
                Show();
                MessageBox.Show("Заполните пустые поля");
            }
            else
            {

                book.TitleBook = textBox1.Text;
                book.PublicationDateBook = Int32.Parse(textBox2.Text);
                books = booksql.showBooks();

                book.Id = n;

                AuthorSQl authorsql = new AuthorSQl();
                book.Author.Surname = textBox3.Text;
                book.Author.Name = textBox4.Text;
                book.Author.MiddleName = textBox5.Text;
                authors = authorsql.showAuthors();

                for (int m = 0; m < books.Length; m++)
                {
                    if (books[m].Id == n)
                    {
                        i = books[m].Author.Id;
                    }

                }

                authorsql.update(book.Author, i);
                booksql.update(book, n);
            
            Close();
        }
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
