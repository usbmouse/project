﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class BooksAuthors
    {
        int idBooksAuthors;

        public int IDBooksAuthors
        {
            get { return idBooksAuthors; }
            set { idBooksAuthors = value; }
        }
        int idBook;

        public int IDBook
        {
            get { return idBook; }
            set { idBook = value; }
        }
        int idAuthor;

        public int IDAuthor
        {
            get { return idAuthor; }
            set { idAuthor = value; }
        }
    }
}
