﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Library
{
    class BooksAuthorsSQL
    {   string select = "select * from BooksAuthors";
        string insertSt = "insert into BooksAuthors" + "(IDBook, IDAuthor) values(@IDBook,@IDAuthor)";
        string sql = "Data Source=MY_COMPUTER;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update BooksAuthors set IDBook = @IDBook, IDAuthor =@IDAuthor where idBooksAuthors=@idBooksAuthors";
        string removeSt = "delete from BooksAuthors where idBooksAuthors = @idBooksAuthors";
        string record = "select idBooksAuthors from BooksAuthors where ";
        
        DataTable datatable;

        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public BooksAuthors[] showBooksAuthors(SqlConnection sqlc)
        {
            
            BooksAuthors booksAuthors = new BooksAuthors();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlc);
            DataSet set = new DataSet();
            adapter.Fill(set,"BooksAuthors");
            datatable = set.Tables[0];
            BooksAuthors[] booksAuthorsMasiv = new BooksAuthors[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                booksAuthors = new BooksAuthors();
                row = datatable.Rows[i];
                booksAuthors.IDBooksAuthors = (int) row["IDBooksAuthors"];
                booksAuthors.IDBook = (int) row["IDBook"];
                booksAuthors.IDAuthor = (int) row["IDAuthor"];

                booksAuthorsMasiv[i] = booksAuthors;   

            }

            return booksAuthorsMasiv;

        }
        public int insert(SqlConnection sqlc,int idBook, int idAuthor)
        {
            BooksAuthors booksAuthors = new BooksAuthors();
            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlc);
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@IDBook";
            param.Value = idBook ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@IDAuthor";
            param.Value = idAuthor;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
            }

            sqlc.Close();

            return 1;
        }
        public int update(SqlConnection sqlc, int idBooksAuthors, int idBook, int idAuthor)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlc);

            SqlParameter param = new SqlParameter();
            
            param.ParameterName = "@IDBooksAuthors";
            param.Value = idBooksAuthors ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter(); 
                   
            param.ParameterName = "@IDBook";
            param.Value = idBook ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@IDAuthor";
            param.Value = idAuthor;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlc.Close();

            return 1;
        }
        public int remove(SqlConnection sqlc, int idBooksAuthors)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlc);

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idBooksAuthors";
            param.Value = idBooksAuthors ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
            }

            sqlc.Close();

            return 1;    
        }
    }
}

    

