﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
   public class BooksUsers
    {
        string authorLastName;

        public string AuthorLastName
        {
            get { return authorLastName; }
            set { authorLastName = value; }
        }
        
        
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
        
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        string surname;

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        private string middleName;

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }
        int idLog;

        public int IdLog
        {
            get { return idLog; }
            set { idLog = value; }
        }

        int idBook;

        public int IdBook
        {
            get { return idBook; }
            set { idBook = value; }
        }

        private int publicationDateBook;

        public int PublicationDateBook
        {
            get { return publicationDateBook; }
            set { publicationDateBook = value; }
        }
        DateTime issueDateBook;

        public DateTime IssueDateBook
        {
            get { return issueDateBook; }
            set { issueDateBook = value; }
        }
        DateTime returnDateBook;

        public DateTime ReturnDateBook
        {
            get { return returnDateBook; }
            set { returnDateBook = value; }
        }
       

       }
}
