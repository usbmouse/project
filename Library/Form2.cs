﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            JournalSQl journal = new JournalSQl();
            BookSQl book = new BookSQl();
            UserSQl user = new UserSQl();
            Book[] books = book.showBooks();
            User[] users = user.showUsers();
            bindingSource2.DataSource = books;
            bindingSource3.DataSource = users;
            bindingSource1.DataSource = journal.showJournal(books,users);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            JournalSQl journal = new JournalSQl();
            BookSQl book = new BookSQl();
            UserSQl user = new UserSQl();
            Book[] books = book.showBooks();
            User[] users = user.showUsers();
            
            dataGridView1.DataSource = journal.showJournal(books,users);
            //bindingSource1.DataSource  = journal.showJournal(journal.sqlConnection(), books, users);
            //dataGridView1.DataSource = bindingSource1;

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            JournalSQl journal = new JournalSQl();
            BookSQl book = new BookSQl();
            UserSQl user = new UserSQl();
            Book[] books = book.showBooks();
            User[] users = user.showUsers();
            Journal journals = new Journal();
            journals.Book = books[dataGridView2.CurrentRow.Index];
            journals.User = users[dataGridView3.CurrentRow.Index ];
            journals.IssueDateBook = dateTimePicker1.Value;
            journals.ReturnDateBook = dateTimePicker2.Value;
            journal.insert(journals.IssueDateBook,journals.ReturnDateBook,journals.Book.Id ,journals.User.Id);
            bindingSource1.DataSource = journal.showJournal(books,users);
            dataGridView1.DataSource = bindingSource1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            JournalSQl journal = new JournalSQl();
            BookSQl book = new BookSQl();
            UserSQl user = new UserSQl();
            Book[] books = book.showBooks();
            User[] users = user.showUsers();
            Journal journals = new Journal();
            journals.Book = books[dataGridView3.CurrentRow.Index];
            journals.User = users[dataGridView2.CurrentRow.Index];
            journals.IssueDateBook = dateTimePicker1.Value;
            journals.ReturnDateBook = dateTimePicker2.Value;

            int id;
            id = (int) dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].Value;
            journal.update(journals.IssueDateBook, journals.ReturnDateBook, journals.Book.Id, journals.User.Id ,id);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            JournalSQl journal = new JournalSQl();
            int id;
            id = (int)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].Value;
            journal.remove(id);


        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
