﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Library
{
    public class BookSQl
    {
        string select = "select * from Books order by IDBook";
        string insertSt = "insert into Books" + "(TitleBook,PublicationDateBook) values(@TitleBook,@PublicationDateBook)";
        //string sql = "Data Source=MY_COMPUTER;Initial Catalog=Library;Integrated Security=True";
        string updateSt = "update Books set TitleBook = @TitleBook, PublicationDateBook =@PublicationDateBook where idBook=@idBook;";
        string removeSt = "delete from Books where IDBook = @idBook";
        //string searchBook = "select * from Books where TitleBook like '%:TitleBook%'" ;
        string sql = @"Data Source=.\S;Initial Catalog=Library;Integrated Security=True";

        string getAuthorsByBookId = "select * from BooksAuthors where IDBook = @idBook";

        string removeBooksAuthors = "delete from BooksAuthors where IDBook = @idBook";

        DataTable datatable;


        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public Book[] showBooks()
        {
            
            Book book = new Book();
            
            AuthorSQl a1 = new AuthorSQl();
            Author[] a2 = a1.showAuthors();
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlConnection());
            DataSet set = new DataSet();
            adapter.Fill(set,"Books");
            datatable = set.Tables[0];
            Book[] books = new Book[datatable.Rows.Count];
            


            DataRow row;


            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                book = new Book();
                row = datatable.Rows[i];
                book.Id = (int) row["idBook"];
                book.PublicationDateBook = (int) row["PublicationDateBook"];
                book.TitleBook = (string) row["TitleBook"];
                book.Author = a2[i];
                books[i] = book;   

            }

            return books;

        }
        public int insert(Book book)
        {
           /* SqlCommand sqlCommand1 = new SqlCommand("insert into BooksAuthors" + "(IDBook, IDAuthor) values('"+book.Id +"','"+book.Author.Id +"')",sqlConnection());

            try
            {
                sqlCommand1.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
                //  return;
            }
            */


            SqlCommand sqlCommand = new SqlCommand(insertSt,sqlConnection());
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@TitleBook";
            param.Value =book.TitleBook ;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@PublicationDateBook";
            param.Value = book.PublicationDateBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
              //  return;
            }

            Book[] books;
            Author[] authors;
            BookSQl booksql = new BookSQl();
            AuthorSQl authorsql = new AuthorSQl();
            books = booksql.showBooks();
            authors = authorsql.showAuthors();
            int authorId = authors[authors.Length - 1].Id;
            int bookId = books[books.Length - 1].Id;
            SqlCommand sqlCommand1 = new SqlCommand("insert into BooksAuthors" + "(IDBook, IDAuthor) values('" + bookId + "','" + authorId + "')", sqlConnection());

            try
            {
                sqlCommand1.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
                //  return;
            }
            

            return 1;
        }
        public int update(Book book,int id)
        {
            
            SqlCommand sqlCommand = new SqlCommand(updateSt, sqlConnection());

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idBook";
            param.Value = book.Id;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@TitleBook";
            param.Value = book.TitleBook ;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@PublicationDateBook";
            param.Value = book.PublicationDateBook ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
                //  return;
            }

            sqlConnection().Close();

            return 1;
        }
        public int remove( int id)
        {
            Book book = new Book();
            SqlCommand sqlCommand = new SqlCommand(removeSt, sqlConnection());

            AuthorSQl authorSQl = new AuthorSQl();
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@idBook";
            param.Value = id ;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                 MessageBox.Show(es.Message);
                //  return;
            }

            SqlCommand sqlCommand2 = new SqlCommand(getAuthorsByBookId, sqlConnection());
            SqlParameter param2= new SqlParameter();
            param2.ParameterName = "@idBook";
            param2.Value = id;
            param2.SqlDbType = SqlDbType.Int;
            sqlCommand2.Parameters.Add(param2);
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand2);
            DataSet set = new DataSet();
            adapter.Fill(set, "BooksAuthors");
            DataTable datatable2 = new DataTable();
            datatable2 = set.Tables[0];
            int authorsId;
            var b = false;
            for (int i = 0; i < datatable2.Rows.Count; i++)
            {
                authorsId = (int)datatable2.Rows[i]["IDAuthor"];
                if (!b)
                {
                    authorSQl.remove(authorsId);
                    b = true;
                }
            }

            SqlCommand sqlCommand3 = new SqlCommand(removeBooksAuthors, sqlConnection());
            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@idBook";
            param3.Value = id;
            param3.SqlDbType = SqlDbType.Int;
            sqlCommand3.Parameters.Add(param3);
            try
            {
                sqlCommand3.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message);
            }
            
            sqlConnection().Close();
            return 1;
        }

        public Book[] search(string title)
        {

            string searchBook = "select * from Books where TitleBook like '%"+title+"%'";
            Book book = new Book();

            AuthorSQl a1 = new AuthorSQl();
            Author[] a2 = a1.showAuthors();

            SqlDataAdapter adapter = new SqlDataAdapter(searchBook, sqlConnection());
            DataSet set = new DataSet();
            adapter.Fill(set, "Books");
            datatable = set.Tables[0];
            Book[] books = new Book[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                book = new Book();
                row = datatable.Rows[i];
                book.Id = (int)row["idBook"];
                book.PublicationDateBook = (int)row["PublicationDateBook"];
                book.TitleBook = (string)row["TitleBook"];
                book.Author = a2[i];
                books[i] = book;

            }

            return books;
       
        }
    }
}
